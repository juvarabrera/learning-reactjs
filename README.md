# learning-reactjs

## Background

I started getting exposed with ReactJS when our startup used ReactJS to create our web application. I am already familiar on how it works, how it builds and runs, but I can say that I am not an expert on ReactJS yet. My goal is to be an expert in create web applications using ReactJS.

Even though, I lead multiple teams using ReactJS and AngularJS, and I'm just familiar with those two frameworks, I still want to create applications with ReactJS first by myself.

## Where I am learning
I am learning ReactJS from Codecademy (https://www.codecademy.com/courses/react-101/)

## Resources
[The Virtual DOM](https://www.codecademy.com/article/react-virtual-dom)
[React Top-Level API](https://reactjs.org/docs/react-api.html#reactcomponent)