import React from 'react';
import ReactDOM from 'react-dom';

const fiftyFifty = Math.random() < 0.5;

// New component class starts here:
class TonightsPlan extends React.Component {
  render() {
    let text = "Tonight I'm going ";
    if(fiftyFifty) {
      text += "out WOOO";
    } else {
      text += "to bed WOOO";
    }
    return <h1>{text}</h1>;
  }
}

ReactDOM.render(
  <TonightsPlan/>,
  document.getElementById('app')
);