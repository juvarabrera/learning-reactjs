import React from 'react';
import ReactDOM from 'react-dom';

// Write code here:
const myList = (
  <ul>
    <li>Hello</li>
    <li>World</li>
  </ul>
)

ReactDOM.render(myList, document.getElementById("app"));